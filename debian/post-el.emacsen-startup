;; -*-emacs-lisp-*-
;;
;; Emacs startup file for post-el package
;;
;; Shamelessly stolen from the emacs-goodies-el package.

;; We have to add this to the load-path:
(if (fboundp 'debian-pkg-add-load-path-item) 
    (setq load-path (debian-pkg-add-load-path-item
                     (concat "/usr/share/"
                             (symbol-name flavor)
                             "/site-lisp/post-el")) )
  (setq load-path (cons (concat "/usr/share/"
                                (symbol-name flavor)
                                "/site-lisp/post-el") load-path)))

; load for now, fix it for autoloading later
;(load "post")

(autoload 'post-mode "post" "Major mode for composing email or news with an external agent.
To customize it, type \\[customize] and select [Applications] [Mail] [Post].
When you finish editing this message, type \\[post-save-current-buffer-and-exit] to save and exit Emacs.

\\{post-mode-map}" t)

(defgroup post nil
  "Composing e-mail messages with Post.
Emacs can run as an external editor for Mutt, the spiffy Unix mail reader
du jour, or slrn, the spiffy Unix news reader du jour.  You can get
Mutt from http://www.mutt.org/."
  :group 'mail)

(defcustom post-mail-message "\\(mutt-[a-zA-Z0-9-.]+-[0-9]+-[0-9]+\\(-[a-fA-F0-9]+\\)?\\|mutt[a-zA-Z0-9._-]\\{6\\}\\)\\'"
  "*Regular expression which matches your mailer's temporary files."
  :type 'regexp
  :group 'post)

(defcustom post-news-posting "\\.\\(followup\\|letter\\|article\\)$"
  "*Regular expression which matches your news reader's composition files"
  :type 'string
  :group 'post)

(defcustom post-insert-to-auto-mode-alist-on-load t
  "Automatically insert post-mode with post-mail-message to auto-mode-alist"
  :type 'boolean
  :group 'post)

(when post-insert-to-auto-mode-alist-on-load
  (unless (assq post-mail-message auto-mode-alist)
    (setq auto-mode-alist
	  (cons (cons post-mail-message 'post-mode)
		auto-mode-alist)))
  (unless (assq post-news-posting auto-mode-alist)
    (setq auto-mode-alist
	  (cons (cons post-news-posting 'post-mode)
		auto-mode-alist))))
